import React, {Component} from 'react';
import axios from "../../axios-quotes";
import {CATEGORIES} from "../../Categories";
import Nav from "../UI/Nav";
import "./Form.css"
class Edit extends Component {
    state = {
        category: Object.keys(CATEGORIES)[0],
        text :'',
        author:'',
    };
    valueChanged = event =>{
        const {name,value} = event.target;
        this.setState({[name]: value});
    };
    componentDidMount() {
        const id=this.props.match.params.id;
        axios.get(`/quote/${id}.json`).then(response=>{
            this.setState({ category: response.data.category,
                                  text: response.data.text,
                                  author: response.data.author,});
        })
    }

    publishHandler = () => {
        const id=this.props.match.params.id;
        axios.put(`/quote/${id}.json`, this.state).then(response=>{
            this.props.history.push('/');
        })
    }

    render() {
        return (
            <div className="new-quote">
                <Nav/>
                <form className="quote-form" onSubmit={this.submitHandler}>

                    <p>Category</p>
                    <select name="category" onChange={this.valueChanged} value={this.state.category}>
                        {Object.keys(CATEGORIES).map(catId =>(
                            <option key={catId} value={catId}>{CATEGORIES[catId]}</option>
                        ))}
                    </select>

                    <p>Author</p>
                    <input type="text" value={this.state.author}
                           onChange={this.valueChanged}
                           className="quote-author"
                           name="author"/>

                    <p>Quote text:</p>
                    <textarea value={this.state.text}
                              onChange={this.valueChanged}
                              className="quote-text" a cols="30" rows="10"
                              name="text"/>

                    <button className="add-quote-btn">Submit</button>
                </form>
            </div>
        );
    }
}


export default Edit;
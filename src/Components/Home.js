import React, {Component, Fragment} from 'react';
import Nav from "./UI/Nav";
import {NavLink} from 'react-router-dom'
import axios from '../axios-quotes';
import {CATEGORIES} from "../Categories";

class Home extends Component {
    state = {
        quotes: []
    }

    componentDidMount(){
        axios.get('/quote.json').then(response =>{
            let quotes = [];
            for(let key in response.data) {
                quotes.push({category: response.data[key].category ,
                             text: response.data[key].text,
                             author:response.data[key].author,
                             id: key})
            }
            this.setState({quotes})
        })

    };
    goToEdit=(id) => {
        this.props.history.push(`/${id}/edit`)
    };
    remove=(id)=> {
        axios.delete(`/quote/${id}.json`).then(() => {
            axios.get('/quote.json').then(response =>{
                let quotes = [];
                for(let key in response.data) {
                    quotes.push({category: response.data[key].category ,
                        text: response.data[key].text,
                        author:response.data[key].author,
                        id: key})
                }
                this.setState({quotes})
            })
        })
    };

    render() {
        return (
            <Fragment>
                <h2>Quotes Bay</h2>
                {Object.keys(CATEGORIES).map(catId =>(
                    <div key={catId} className="cat-links">
                        <NavLink to={"/quote/" + catId} exact >{CATEGORIES[catId]}</NavLink>
                    </div>
                        ))}
                <Nav/>
                {this.state.quotes.map((quotes,index)=>{
                return(
                <div key={quotes.id} className="quote-box">
                <p>{quotes.category}</p>
                <p>{quotes.text}</p>
                <h4>
                {quotes.author}
                </h4>
                <button className="del-btn" onClick={()=>{this.remove(quotes.id)}}>X</button>
                <button className="edit-btn" onClick={()=>{this.goToEdit(quotes.id)}}>Edit</button>
                </div>
                )
                })}
            </Fragment>
        );
    }
}

export default Home;
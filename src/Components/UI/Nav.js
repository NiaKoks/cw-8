import React, {Component} from 'react';
import {NavLink} from "react-router-dom";
import './Nav.css'
class Nav extends Component {
    render() {
        return (
            <div className='nav'>
                <NavLink to="/">Home</NavLink>
                <NavLink to='/add'>Add</NavLink>
            </div>
        );
    }
}

export default Nav;
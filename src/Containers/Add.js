import React, {Component,Fragment} from 'react';
import axios from '../axios-quotes';
import Quote from '../Components/Quote.js'
class Add extends Component {
    addQuote = quote =>{
        axios.post('quote.json',quote).then(()=>{
            this.props.history.replace('/')
        })
    }
    render() {
        return (
            <Fragment>
                <h2>Add new Quote</h2>
                <Quote onSubmit={this.addQuote}/>
            </Fragment>
        );
    }
}

export default Add;
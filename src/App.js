import React, { Component } from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';

import Home from './Components/Home';
import Add from './Containers/Add'
import Quote from './Components/Quote'
import './App.css';
import Edit from "./Components/Servises/Edit";

class App extends Component {
  render() {
    return (
        <BrowserRouter>
          <Switch>
            <Route path="/" exact component={Home}></Route>
            <Route path="/add" component={Add}></Route>
            <Route path="/quotes/:id" component={Quote}></Route>
            <Route path="/:id/edit" component={Edit}></Route>
          </Switch>
        </BrowserRouter>
    );
  }
}

export default App;

import axios from 'axios'

const instance = axios.create({
    baseURL:'https://cw-8-niakoks.firebaseio.com/'
});
export default instance;